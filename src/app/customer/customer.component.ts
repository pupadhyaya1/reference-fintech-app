import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from '../app.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customer: any;
  customers: any;
  newCustomer: any;
  errorMessage: any;

  constructor(private modalService: NgbModal,
    private appService: AppService) { }

  ngOnInit() {
  }

  openGetCustomerModal(getCustomerRequestModal) {
    this.modalService.open(getCustomerRequestModal, { size: 'lg' });
  }

  getCustomer(form, getCustomerResponseModal) {
    var customerId = (form.value.customerId != "") ? form.value.customer : "98:196105238301803";

    this.modalService.dismissAll();
    this.customer = this.appService.getCustomer(customerId).subscribe(
      (customer) => {
        this.customer = customer
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(getCustomerResponseModal, { size: 'lg' });
  }

  openCreateCustomer(createCustomerRequestModal) {
    this.modalService.open(createCustomerRequestModal, { ariaLabelledBy: 'createCustomerRequestModal', size: 'lg', centered: true });
  }

  createCustomer(form, createCustomerResponseModal) {
    var name = (form.value.name != "") ? form.value.name : "UMB";
    var address1 = (form.value.address1 != "") ? form.value.address1 : "1010 Test Avenue";
    var address2 = (form.value.address2 != "") ? form.value.address2 : "Suite 123";
    var city = (form.value.city != "") ? form.value.city : "Kansas City";
    var state = (form.value.state != "") ? form.value.state : "MO";
    var zip = (form.value.zip != "") ? form.value.zip : "12345";
    var tin = (form.value.tin != "") ? form.value.tin : "000000000";

    this.modalService.dismissAll();
    this.newCustomer = this.appService.createCustomer(name, address1, address2, city, state, zip, tin).subscribe(
      (newCustomer) => {
        this.newCustomer = newCustomer
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(createCustomerResponseModal, { ariaLabelledBy: 'createCustomerResponseModal', size: 'lg', centered: true });
  }

  openListCustomers(listCustomersRequestContent) {
    this.modalService.open(listCustomersRequestContent, { ariaLabelledBy: 'listCustomersRequestModal', size: 'lg', centered: true });
  }

  listCustomers(form, listCustomersResponseContent) {
    var accountNumber = form.value.accountNumber;

    this.modalService.dismissAll();
    this.customers = this.appService.listCustomers(accountNumber).subscribe(
      (customers) => {
        this.customers = customers
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(listCustomersResponseContent, { ariaLabelledBy: 'listCustomersResponseContent' });
  }


}
