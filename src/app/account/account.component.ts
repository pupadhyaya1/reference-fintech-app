import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from '../app.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  account: any;
  accounts: any;
  transactions: any;
  newAccount: any;
  errorMessage: any;
  productCode: string;

  constructor(private modalService: NgbModal,
    private appService: AppService) { }

  ngOnInit() {
    this.productCode = 'SAV';
  }

  openCreateAccount(createAccountRequestContent) {
    this.modalService.open(createAccountRequestContent, { ariaLabelledBy: 'createAccountRequestContent', size: 'lg', centered: true });
  }

  createAccount(form, accountResponseContent) {
    var productCode = (form.value.productCode != "") ? form.value.productCode : "SAV";
    var bankNumber = (form.value.bankNumber != "") ? form.value.bankNumber : "98";

    this.modalService.dismissAll();
    this.newAccount = this.appService.createAccount(productCode, bankNumber).subscribe(
      (newAccount) => {
        this.newAccount = newAccount
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(accountResponseContent, { ariaLabelledBy: 'accountResponseContent', size: 'lg', centered: true });
  }

  openGetAccountModal(getAccountRequestModal) {
    this.modalService.open(getAccountRequestModal, { ariaLabelledBy: 'getAccountRequestModal', size: 'lg', centered: true });
  }

  getAccount(form, getAccountResponseModal) {
    var accountId = (form.value.accountId != "") ? form.value.accountId : "978:DDA:20097091878";

    this.modalService.dismissAll();
    this.account = this.appService.getAccount(accountId).subscribe(
      (account) => {
        this.account = account
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(getAccountResponseModal, { ariaLabelledBy: 'getAccountResponseModal', size: 'lg', centered: true });
  }
  openListAccountsModal(listAccountsRequestModal) {
    this.modalService.open(listAccountsRequestModal, { ariaLabelledBy: 'listAccountsRequestModal', size: 'lg', centered: true });
  }

  listAccounts(form, listAccountsResponseModal) {
    var customerId = (form.value.customerId != "") ? form.value.customerId : "98:196105238301803";

    this.modalService.dismissAll();
    this.accounts = this.appService.listAccounts(customerId).subscribe(
      (accounts) => {
        this.accounts = accounts
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(listAccountsResponseModal, { ariaLabelledBy: 'listAccountsResponseModal', size: 'lg', centered: true });
  }

  openListTransactions(listTransactionsRequestContent) {
    this.modalService.open(listTransactionsRequestContent, { ariaLabelledBy: 'listTransactionsRequestContent', size: 'lg', centered: true });
  }

  listTransactions(form, listTransactionsResponseContent) {
    var accountId = (form.value.accountId != "") ? form.value.accountId : "98:DDA:98771288888999";
    var startDate = (form.value.startDate != "") ? form.value.startDate : '2019-03-25T16:48:45.0910858-05:00';
    var endDate = (form.value.endDate != "") ? form.value.endDate : '2019-03-30T16:48:45.0910858-05:00';

    this.modalService.dismissAll();
    this.transactions = this.appService.listTransactions(accountId, startDate, endDate).subscribe(
      (transactions) => {
        this.transactions = transactions
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(listTransactionsResponseContent, { ariaLabelledBy: 'listTransactionsResponseContent', size: 'lg', centered: true });
  }

}
