import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AppService {
  private createCustomerURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Customer/CreateCustomer';
  private getCustomerURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Customer/GetCustomer';
  private listCustomersURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Customer/ListCustomers';

  private createAccountURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Account/CreateAccount';
  private getAccountURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Account/GetAccount';
  private listAccountsURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Account/ListAccounts';
  private listTransactionsURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Account/ListTransactions';

  private getLocationURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Channel/GetLocation';
  private listLocationsURL = 'https://api.us-south.apiconnect.appdomain.cloud/kevinhe-umb-training-kevinhe-space-1/corebanking/api/Channel/ListLocations';

  appOnehttpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'X-IBM-Client-Id': '50d490f3-157f-482a-b5c8-c869e92b0dcb',
        'X-IBM-Client-Secret': 'Y0qV6pR4oH4gE5wE8pS7xO5aQ7oC1bE3dS8bB8wS4jH2gH8gT6'
      }
    )
  };

  appTwohttpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'X-IBM-Client-Id': 'ee39366f-9de4-41e6-8679-c55ac5b6b552',
        'X-IBM-Client-Secret': 'H1uF8fC3eC7rI4nT7cY1nU5kQ4mY4nV5yF6vX2hG1pH3qO5iH0'
      }
    )
  }

  constructor(private http: HttpClient) { }

  createCustomer(name, address1, address2, city, state, zip, tin): Observable<String> {
    var body = JSON.stringify({
      "Customer": {
        "CustomerTaxId": {
          "Value": tin,
          "SecureType": "None"
        },
        "CustomerPhones": [
          {
            "PhoneType": "Work",
            "IsActive": true,
            "Number": "816-860-7000"
          }
        ],
        "CustomerAddresses": [
          {
            "AddressLine1": address1,
            "AddressLine2": address2,
            "AddressCity": city,
            "AddressState": state,
            "AddressPostalCode": zip,
            "AddressCountry": "USA",
            "IsAddressActive": true,
            "AddressType": "Primary"
          }
        ],
        "CustomerType": "Company",
        "CustomerBranch": {
          "Number": "35"
        },
        "Company": {
          "CompanyName": {
            "Name": name
          },
          "EstablishedDate": "1913-01-01T00:00:00"
        },
        "Officers": [
          {
            "Code": "NotReal",
            "StartDate": "0001-01-01T00:00:00"
          }
        ],
        "WithHoldCode": "PF"
      },
      "RequestSource": {
        "RequestId": "76738868-b7cd-4e7b-a5b9-5b84c564439b",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.createCustomerURL, body, this.appOnehttpOptions);
  }

  getCustomer(customerId): Observable<String> {
    var body = JSON.stringify({
      "Criteria": {
        "Identifier": {
          "Id": customerId,
          "IdentifierType": "CISCustomerNumber"
        }
      },
      "RequestSource": {
        "RequestId": "76738868-b7cd-4e7b-a5b9-5b84c564439b",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.getCustomerURL, body, this.appOnehttpOptions);
  }

  listCustomers(accountNumber): Observable<String> {
    var body = JSON.stringify({
      "Page": "0",
      "NumberRecordsPerPage": 10,
      "CustomerCriteria": {
        "AccountNumber": {
          "Value": accountNumber,
          "SecureType": "None"
        }
      },
      "RequestSource": {
        "RequestId": "8267b72e-08a4-431d-83f8-3856aa1220ff",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.listCustomersURL, body, this.appTwohttpOptions);
  }

  createAccount(productCode, bankNumber): Observable<any> {
    var body = JSON.stringify({
      "Account": {
        "AccountProduct": {
          "ProductCode": productCode,
          "SubProductCode": "R1"
        },
        "AccountRoutingNumber": {
          "Value": "101000165",
          "SecureType": "None"
        },
        "AccountBranch": {
          "Number": "35",
          "Bank": {
            "BankNumber": bankNumber
          }
        },
        "Officers": [
          {
            "Code": "NotReal",
            "StartDate": "0001-01-01T00:00:00"
          }
        ],
        "AccountAttributes": [
          {
            "Key": "TaxId",
            "Value": "55555555"
          },
          {
            "Key": "AccountPlan",
            "Value": "RR1"
          }
        ],
        "TitleLines": [
          {
            "Title": "Josh Cramer",
            "Sequence": 0
          }
        ],
        "FundsOwnerCode": "_01",
        "WithHoldingCode": "PF"
      },
      "RequestSource": {
        "RequestId": "1d8d3902-c451-498a-8cac-ca76d00b8f04",
        "SourceType": "Teller",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.createAccountURL, body, this.appOnehttpOptions);
  }

  getAccount(id): Observable<String> {
    var body = JSON.stringify({
      "Criteria": {
        "Identifier": {
          "Id": id,
          "IdentifierType": "AccountId"
        }
      },
      "RequestSource": {
        "RequestId": "76738868-b7cd-4e7b-a5b9-5b84c564439b",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });
    return this.http.post<String>(this.getAccountURL, body, this.appTwohttpOptions);
  }

  listAccounts(customerId): Observable<String> {
    var body = JSON.stringify({
      "AccountCriteria": {
        "CustomerId": {
          "Id": customerId,
          "IdentifierType": "CISCustomerNumber"
        }
      },
      "RequestSource": {
        "RequestId": "aa852863-06b1-4bf2-90cd-103472d91c84",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.listAccountsURL, body, this.appOnehttpOptions);
  }

  listTransactions(accountId, startDate, endDate): Observable<String> {
    var body = JSON.stringify({
      "Page": "0",
      "NumberRecordsPerPage": 10,
      "TransactionCriteria": {
        "AccountId": {
          "Id": accountId,
          "IdentifierType": "AccountId"
        },
        "TransactionDateRange": {
          "StartDate": startDate,
          "EndDate": endDate
        },
        "IncludePending": true
      },
      "RequestSource": {
        "RequestId": "58b804ea-a20d-4128-a0ab-39fc68ad24d0",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.listTransactionsURL, body, this.appTwohttpOptions);
  }

  getLocation(locationId): Observable<String> {
    var body = JSON.stringify({
      "Criteria": {
        "Identifier": {
          "Id": locationId,
          "IdentifierType": "RioLocationId"
        }
      },
      "RequestSource": {
        "RequestId": "12dc4da1-b3e7-4ee7-8fc6-b3adfe224580",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.getLocationURL, body, this.appTwohttpOptions);
  }

  listLocations(searchLocation, locationType): Observable<String> {
    var body = JSON.stringify({
      "Page": "0",
      "NumberRecordsPerPage": 10,
      "LocationCriteria": {
        "SearchLocation": searchLocation,
        "LocationType": locationType,
        "Radius": 50
      },
      "RequestSource": {
        "RequestId": "0400ba19-43fc-45ab-915d-086202d52e40",
        "SourceType": "Web",
        "ApiKey": {
          "Key": "XXXXXXX",
          "Secret": "XXXXXX"
        },
        "User": {
          "Token": "XXXXXXXXXXXX"
        }
      }
    });

    return this.http.post<String>(this.listLocationsURL, body, this.appOnehttpOptions);
  }
}