import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppService } from '../app.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  location: any;
  locations: any;
  errorMessage: any;
  locationType: string;

  constructor(private modalService: NgbModal,
    private appService: AppService) { }

  ngOnInit() {
    this.locationType = "All"
  }

  openGetLocationModal(getLocationRequestModal) {
    this.modalService.open(getLocationRequestModal, { ariaLabelledBy: 'getLocationRequestModal', size: 'lg', centered: true });
  }

  getLocation(form, getLocationResponseModal) {
    var locationId = (form.value.locationId != "") ? form.value.locationId : "101";

    this.modalService.dismissAll();
    this.location = this.appService.getLocation(locationId).subscribe(
      (location) => {
        this.location = location
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(getLocationResponseModal, { ariaLabelledBy: 'getLocationResponseModal', size: 'lg', centered: true });
  }

  openListLocations(listLocationsRequestContent) {
    this.modalService.open(listLocationsRequestContent, { ariaLabelledBy: 'listLocationsRequestContent', size: 'lg', centered: true });
  }

  listLocations(form, listLocationsResponseContent) {
    var searchLocation = (form.value.searchLocation != "") ? form.value.searchLocation : "123 Test Street";
    var locationType = form.value.locationType;

    this.modalService.dismissAll();
    this.locations = this.appService.listLocations(searchLocation, locationType).subscribe(
      (locations) => {
        this.locations = locations
      },
      (error) => {
        this.errorMessage = error.error.moreInformation;
      });
    this.modalService.open(listLocationsResponseContent, { ariaLabelledBy: 'listLocationsResponseContent', size: 'lg', centered: true });
  }

}
